package weather

import (
	"encoding/json"

	owm "github.com/briandowns/openweathermap"
)

type WeatherData struct {
	Sunrise     *int    `json:"sunrise,omitempty"`
	Sunset      *int    `json:"sunset,omitempty"`
	CloudsAll   int     `json:"clouds_all"`
	Description string  `json:"description"`
	Temp        float64 `json:"temp"`
	TempMin     float64 `json:"temp_min"`
	TempMax     float64 `json:"temp_max"`
	FeelsLike   float64 `json:"feels_like"`
	Pressure    float64 `json:"pressure"`
	Humidity    int     `json:"humidity"`
	WindSpeed   float64 `json:"wind_speed"`
	WindDeg     float64 `json:"wind_deg"`
	RainOneH    float64 `json:"rain_1h,omitempty"`
	RainThreeH  float64 `json:"rain_3h,omitempty"`
	SnowOneH    float64 `json:"snow_1h,omitempty"`
	SnowThreeH  float64 `json:"snow_3h,omitempty"`
}

func FromCurrent(data *owm.CurrentWeatherData) WeatherData {
	return WeatherData{
		Sunrise:     &data.Sys.Sunrise,
		Sunset:      &data.Sys.Sunset,
		CloudsAll:   data.Clouds.All,
		Description: data.Weather[0].Description,
		Temp:        data.Main.Temp,
		TempMin:     data.Main.TempMin,
		TempMax:     data.Main.TempMax,
		FeelsLike:   data.Main.FeelsLike,
		Pressure:    data.Main.Pressure,
		Humidity:    data.Main.Humidity,
		WindSpeed:   data.Wind.Speed,
		WindDeg:     data.Wind.Deg,
		RainOneH:    data.Rain.OneH,
		RainThreeH:  data.Rain.ThreeH,
		SnowOneH:    data.Snow.OneH,
		SnowThreeH:  data.Snow.ThreeH,
	}
}

func FromForecast(data *owm.Forecast5WeatherList) WeatherData {
	return WeatherData{
		CloudsAll:   data.Clouds.All,
		Description: data.Weather[0].Description,
		Temp:        data.Main.Temp,
		TempMin:     data.Main.TempMin,
		TempMax:     data.Main.TempMax,
		FeelsLike:   data.Main.FeelsLike,
		Pressure:    data.Main.Pressure,
		Humidity:    data.Main.Humidity,
		WindSpeed:   data.Wind.Speed,
		WindDeg:     data.Wind.Deg,
		RainOneH:    data.Rain.OneH,
		RainThreeH:  data.Rain.ThreeH,
		SnowOneH:    data.Snow.OneH,
		SnowThreeH:  data.Snow.ThreeH,
	}
}

type Weather struct {
	Current  WeatherData `json:"current"`
	Forecast WeatherData `json:"forecast"`
}

func (w Weather) Serialize() (*[]byte, error) {
	content, err := json.Marshal(w)
	if err != nil {
		return nil, err
	}

	return &content, nil
}
