package main

import (
	"os"
	"time"

	owm "github.com/briandowns/openweathermap"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"gitlab.com/DirkFaust/faustility/pkg/conversion"
	"gitlab.com/DirkFaust/faustility/pkg/env"
	"gitlab.com/DirkFaust/faustility/pkg/mqtt"
	"gitlab.com/DirkFaust/weather2mqtt/pkg/weather"
)

const (
	envAPIKey   = "OPEN_WEATHER_API_KEY"
	envLat      = "POSITION_LAT"
	envLon      = "POSITION_LON"
	envHour     = "REQUEST_HOUR"
	envMqttHost = "MQTT_HOST"
	envMqttPort = "MQTT_PORT"
	envLang     = "LANGUAGE"
)

func main() {
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.DebugLevel)

	_ = godotenv.Load()
	apiKey := env.GetOrDie(envAPIKey)

	log.Info("Starting")

	var lat, lon float64
	var mqttPort uint16
	var hour int

	conversion.FromStringOrDie(env.GetOrDie(envLat), &lat)
	conversion.FromStringOrDie(env.GetOrDie(envLon), &lon)
	conversion.FromStringOrDie(env.GetOrDie(envMqttPort), &mqttPort)
	conversion.FromStringOrDie(env.GetOrDie(envHour), &hour)

	mqttHost := env.GetOrDie(envMqttHost)

	lang := os.Getenv(envLang)
	if lang == "" {
		lang = "DE"
	}

	f, err := owm.NewForecast("5", "C", "DE", apiKey)
	if err != nil {
		panic(err)
	}

	w, err := owm.NewCurrent("C", lang, apiKey)
	if err != nil {
		panic(err)
	}

	quotes := make(chan weather.Weather)
	defer close(quotes)
	kill := make(chan bool)
	defer close(kill)

	forwarder, err := mqtt.NewForwarderBuilder[weather.Weather]().
		Channels().Data(quotes).KillWitch(kill).
		Topics().Input("weather2mqtt/command").Output("weather2mqtt/current").
		Server().Host(mqttHost).Port(uint16(mqttPort)).
		Build()

	if err != nil {
		log.Errorf("Unable to build MQTT forwarder: %v", err)
		os.Exit(1)
	}

	ticker := time.NewTicker(time.Minute * 1)
	defer ticker.Stop()
	lastMade := time.Now().Add(-24 * time.Hour)
	go func() {
		for {
			select {
			case <-ticker.C:
				// We just do this once a day at a given hour
				now := time.Now().Local()
				if time.Now().Hour() == hour && lastMade.Day() != now.Day() {
					coords := owm.Coordinates{
						Latitude:  lat,
						Longitude: lon,
					}

					log.Info("Requesting weather now")
					err := w.CurrentByCoordinates(&coords)
					if err != nil {
						log.Errorf("Unable to get current weather: %v", err)
						continue
					}

					// Currently broken
					err = f.DailyByCoordinates(&coords, 5)
					if err != nil {
						log.Errorf("Unable to get forecast weather: %v", err)
						continue
					}

					lastMade = now
					out := weather.Weather{
						Current:  weather.FromCurrent(w),
						Forecast: weather.FromForecast(&f.ForecastWeatherJson.(*owm.Forecast5WeatherData).List[0]),
					}
					log.Infof("Weather: %v", out)
					quotes <- out
				}
			case <-kill:
				log.Infof("Stopping for kill signal")
				return
			}
		}
	}()

	forwarder.Run()
}
