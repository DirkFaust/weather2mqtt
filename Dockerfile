FROM alpine:latest

ARG TARGETOS
ARG TARGETARCH

RUN apk update && \
    apk add tzdata

COPY ./build_${TARGETOS}_${TARGETARCH}/weather2mqtt /opt/

ENV OPEN_WEATHER_API_KEY=00000000-0000-0000-0000-000000000002
ENV POSITION_LAT=52.550289
ENV POSITION_LON=8.585890
ENV REQUEST_HOUR=6
ENV MQTT_HOST=192.168.1.104
ENV MQTT_PORT=1883

USER 1001

CMD /opt/weather2mqtt
